# Cisco Logo Wall Light
This was a project to have an interesting background in my Webex and Telepresence calls from home. It is the Cisco bridge with various colour patterns that can be set by pressing of a button on the controller (I used a Wemos D1 Pro - an old one).

![Cisco Logo Wall Light](images/CiscoLogoLight-AlmostFinished.jpg)
_Figure 1: Prototype Light (Still some wiring work required)_

## Parts
The following parts were used in the project:

 * Wemos Mini D1 Pro (This is an ESP8266 based Micro. Check out wemos.cc, though there are probably newer and better Arduino compatible micros now.)
 * Wemos 1 button shield (Mine was wired to D3 pin, important not to conflict with the pins you will need for the DotPixel strip)
 * DotPixel Led Strip (I used a 71 element strip from RS Components.)
 * 0.12mm hookup wire - Pick a colour that suits your mounting surface.
 * 4 core cable (similar sizing to the hookup wire) again, with sheath colour to suit your environment.
 * USB Cable to suit your micros
 * White PLA filament for the bridge turrent covers
 * Black or appropriate cololured filiment for the turrent bases (that go to the wall)
 * A semi-decent 3D printer :)
 * Soldering Iron and your choice of solder.
 * Hot Glue gun.

## Circuit overview
![Circuit Layout](images/CiscoLogoLight_bb.jpg)
_Figure 2: Circuit_

## Files
There are 2 directories that will be of interest:
  * `fusion360-files` - contains the CAD files for Fusion360 that has the 3 different sized bridge turrent covers and base
  * `arduino` - has the Arduino project file. You will need to set the Button pin and strip pins to suit your micro.

You will need to install the Adafruit DotPixel library from the Arduino IDE library manager, or source it yourself.

 ## Notes
 A few build notes:
  * The DotPixel strip I used had pads between each element that allowed me to cut the strip into arbitrary lengths without waste.
  * I spaced the turrents out at 3.5cm, This is important as you are measuring the hookup cable distances.
  * There is **LOTS** of hot glue in this project to anchor the wire and LED strips in each turrent.
  * I used 4 strands of hookup wire between the LED strips as it was easier to bend, flatten out and shape in and between the turrents.
  * The 4 core cable was used from the end turrent down to the controller.
  * I used Lead solder on the LED strip after trying 2 lead frees, it was __MUCH_ easier to work, and the lower temperature meant I didn't damage the strip as much.
