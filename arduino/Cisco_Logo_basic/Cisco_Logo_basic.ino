// An example demonstrating how to control the Adafruit Dot Star RGB LED
// included on board the ItsyBitsy M4 board.

#include <Adafruit_DotStar.h>
//#include <Serial.h>

// There is only one pixel on the board
#define NUMPIXELS 62

//Use these pin definitions for the ItsyBitsy M4
#define DATAPIN    D2
#define CLOCKPIN   D1

/*
 * Each pilon is a seperate colour.
 */
uint32_t pilon_0[] = {
    7282, 7282, 7282,
    14564, 14564, 14564, 14564, 14564, 14564,
    21845, 21845, 21845, 21845, 21845, 21845, 21845, 21845, 21845, 21845, 21845, 21845, 21845, 21845,
    29127, 29127, 29127, 29127, 29127, 29127,
    36408, 36408, 36408,
    43691, 43691, 43691, 43691, 43691, 43691,
    50972, 50972, 50972, 50972, 50972, 50972, 50972, 50972, 50972, 50972, 50972, 50972, 50972, 50972,
    58254, 58254, 58254, 58254, 58254, 58254, 
    65535, 65535, 65535
};

/*
 * Each pixel tapers from one end of the colour spectrum to the other.
 */
uint32_t pilon_1[] = {
    949, 1899, 2849, 3799, 
    4748, 5698, 6648, 7598, 8548, 9497, 10447, 
    11397, 12347, 13296, 14246, 15196, 16146, 17096, 18045, 18995, 19945, 20895, 21845, 22794, 23744, 24694, 
    25644, 26593, 27543, 28493, 29443, 30393, 31342, 
    32292, 33242, 34192, 35141, 
    36091, 37041, 37991, 38941, 39890, 40840, 41790, 
    42740, 43690, 44639, 45589, 46539, 47489, 48438, 49388, 50338, 51288, 52238, 53187, 54137, 55087, 56037, 
    56986, 57936, 58886, 59836, 60786, 61735, 62685, 
    63635, 64585, 65000, 65534
};

/*
 * All pilons are white.
 */
uint32_t pilon_2[] = {
    65535, 65535, 65535, 
    65535, 65535, 65535, 65535, 65535, 65535, 
    65535, 65535, 65535, 65535, 65535, 65535, 65535, 65535, 65535, 65535, 65535, 65535, 65535, 65535, 
    65535, 65535, 65535, 65535, 65535, 65535, 
    65535, 65535, 65535, 
    65535, 65535, 65535, 65535, 65535, 65535, 
    65535, 65535, 65535, 65535, 65535, 65535, 65535, 65535, 65535, 65535, 65535, 65535, 65535, 65535, 
    65535, 65535, 65535, 65535, 65535, 65535, 
    65535, 65535, 65535
};

uint32_t *pilons[] = { pilon_0, pilon_1, pilon_2 }; //, pilon_1, pilon_2 };

int colour_table = 0;

Adafruit_DotStar strip(NUMPIXELS, DATAPIN, CLOCKPIN, DOTSTAR_BRG);

void setup() {

  Serial.begin(115200);
  Serial.println("Starting...");

  strip.begin(); // Initialize pins for output
  strip.setBrightness(30);

  uint32_t hue=0;
  uint8_t pixel=0;

  for (int i=0; i < NUMPIXELS; i++) {
    if (pilons[colour_table][i] == 65535) {
      strip.setPixelColor(i, strip.gamma32(0xffffff));
    } else {
      strip.setPixelColor(i, strip.gamma32(strip.ColorHSV(pilons[colour_table][i])));
    }
  }
  strip.show();  // Turn all LEDs off ASAP
}

void loop() {
  //rainbow(10);             // Flowing rainbow cycle along the whole strip
}



// Rainbow cycle along whole strip. Pass delay time (in ms) between frames.

void rainbow(int wait) {

  // Hue of first pixel runs 5 complete loops through the color wheel.
  // Color wheel has a range of 65536 but it's OK if we roll over, so
  // just count from 0 to 5*65536. Adding 256 to firstPixelHue each time
  // means we'll make 5*65536/256 = 1280 passes through this outer loop:
  
  for(long firstPixelHue = 0; firstPixelHue < 5*65536; firstPixelHue += 256) {
    for(int i=0; i<strip.numPixels(); i++) { // For each pixel in strip...

      // Offset pixel hue by an amount to make one full revolution of the
      // color wheel (range of 65536) along the length of the strip
      // (strip.numPixels() steps):
      
      int pixelHue = firstPixelHue + (i * 65536L / strip.numPixels());
      
      // strip.ColorHSV() can take 1 or 3 arguments: a hue (0 to 65535) or
      // optionally add saturation and value (brightness) (each 0 to 255).
      // Here we're using just the single-argument hue variant. The result
      // is passed through strip.gamma32() to provide 'truer' colors
      // before assigning to each pixel:
      
      strip.setPixelColor(i, strip.gamma32(strip.ColorHSV(pixelHue)));
    }
    strip.show(); // Update strip with new contents
    delay(wait);  // Pause for a moment
  }
}
