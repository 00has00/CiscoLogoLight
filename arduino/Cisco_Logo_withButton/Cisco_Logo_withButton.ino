// An example demonstrating how to control the Adafruit Dot Star RGB LED
// included on board the ItsyBitsy M4 board.

#include <Adafruit_DotStar.h>
//#include <Serial.h>

/*
 * Some configuration definitions
 */
#define NUMPIXELS 62    // Total Number of Pixels

//Use these pin definitions for the ItsyBitsy M4
#define DATAPIN    D2
#define CLOCKPIN   D1
#define BUTTON     D3

/*
 * Each pilon is a seperate colour.
 */
uint32_t pilon_0[] = {
    7282, 7282, 7282,
    14564, 14564, 14564, 14564, 14564, 14564,
    21845, 21845, 21845, 21845, 21845, 21845, 21845, 21845, 21845, 21845, 21845, 21845, 21845, 21845,
    29127, 29127, 29127, 29127, 29127, 29127,
    36408, 36408, 36408,
    43691, 43691, 43691, 43691, 43691, 43691,
    50972, 50972, 50972, 50972, 50972, 50972, 50972, 50972, 50972, 50972, 50972, 50972, 50972, 50972,
    58254, 58254, 58254, 58254, 58254, 58254, 
    65535, 65535, 65535
};

/*
 * Each pixel tapers from one end of the colour spectrum to the other.
 */
uint32_t pilon_1[] = {
    949, 1899, 2849, 3799, 
    4748, 5698, 6648, 7598, 8548, 9497, 10447, 
    11397, 12347, 13296, 14246, 15196, 16146, 17096, 18045, 18995, 19945, 20895, 21845, 22794, 23744, 24694, 
    25644, 26593, 27543, 28493, 29443, 30393, 31342, 
    32292, 33242, 34192, 35141, 
    36091, 37041, 37991, 38941, 39890, 40840, 41790, 
    42740, 43690, 44639, 45589, 46539, 47489, 48438, 49388, 50338, 51288, 52238, 53187, 54137, 55087, 56037, 
    56986, 57936, 58886, 59836, 60786, 61735, 62685, 
    63635, 64585, 65000, 65534
};

/*
 * All pilons are white.
 */
uint32_t pilon_2[] = {
    65535, 65535, 65535, 
    65535, 65535, 65535, 65535, 65535, 65535, 
    65535, 65535, 65535, 65535, 65535, 65535, 65535, 65535, 65535, 65535, 65535, 65535, 65535, 65535, 
    65535, 65535, 65535, 65535, 65535, 65535, 
    65535, 65535, 65535, 
    65535, 65535, 65535, 65535, 65535, 65535, 
    65535, 65535, 65535, 65535, 65535, 65535, 65535, 65535, 65535, 65535, 65535, 65535, 65535, 65535, 
    65535, 65535, 65535, 65535, 65535, 65535, 
    65535, 65535, 65535
};

uint32_t *pilons[] = { pilon_0, pilon_1, pilon_2 }; //, pilon_1, pilon_2 };

int colour_table = 0;

/*
 * Misc Variables
 */
long int last_int=0; // Last time an interrupt occured.
long int current_int=0; // Set by interrupt handler to current time
int min_button_press_gap = 500; // minimum time between valid button presses.
volatile int run_mode=0;  // Incremented by button interrupt.
int pattern_position=0; // Where are we up to in the rotating pattern?

int debug_lastRunMode = 0;
volatile int debug_intFlag = 0;

/*
 * Button Interrupt (Button on D3)
 */
void ICACHE_RAM_ATTR button_int() {
  current_int = millis();
  if (current_int - last_int > min_button_press_gap ) {
    run_mode += 1;
    if (run_mode >= 5 ) {
      run_mode = 0;
    }
  }
  last_int = current_int;
  debug_intFlag = 1;
}

Adafruit_DotStar strip(NUMPIXELS, DATAPIN, CLOCKPIN, DOTSTAR_BRG);

void setup() {

  Serial.begin(115200);
  Serial.println("Starting...");

  strip.begin(); // Initialize pins for output
  strip.setBrightness(10);

  pinMode(D3, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(D3), button_int, RISING);

  uint32_t hue=0;
  uint8_t pixel=0;

//  for (int i=0; i < NUMPIXELS; i++) {
//    if (pilons[colour_table][i] == 65535) {
//      strip.setPixelColor(i, strip.gamma32(0xffffff));
//    } else {
//      strip.setPixelColor(i, strip.gamma32(strip.ColorHSV(pilons[colour_table][i])));
//    }
//  }
//  strip.show();  // Turn all LEDs off ASAP
}

/*
 * Main Loop - set up run mode, trigger stip updates, manage revolving patterns, send device to sleep for static patterns
 */
void loop() {
  if (debug_intFlag ==1 ) {
    Serial.print("Interrupt Detected - run_mode: ");
    Serial.println(run_mode);
    Serial.print("last_int: ");
    Serial.println(last_int);
    debug_intFlag = 0;
  }
  if (debug_lastRunMode != run_mode) {
    Serial.print("Last Run Mode: ");
    Serial.print(debug_lastRunMode);
    Serial.print(" - Current run mode: ");
    Serial.println(run_mode);
    debug_lastRunMode = run_mode;
  }
  
  if ( run_mode < 3 ) {
    colour_table = run_mode;
    set_strip(0);
  }
  
  if (run_mode == 3 ) {
    colour_table = 1;
    if (++pattern_position >= NUMPIXELS ) pattern_position = 0;
    Serial.println(pattern_position);
    set_strip(pattern_position);
    delay(100);
  }

  if (run_mode == 4 ) {
    strip.clear();
    strip.show();
  }
}



/*
 * Sets up the strip based on the colour table.
 */
void set_strip(int head) {
    int pos=0;
    for (int i=0; i < NUMPIXELS; i++) {
      if ( run_mode == 3 ) {
        pos = i+head;
        if ( pos >= NUMPIXELS ) pos -= NUMPIXELS;
      } else {
        pos = i;
      }
    if (pilons[colour_table][i] == 65535) {
      strip.setPixelColor(pos, strip.gamma32(0xffffff));
    } else {
      strip.setPixelColor(pos, strip.gamma32(strip.ColorHSV(pilons[colour_table][i])));
    }
  }
  strip.show();  // Turn all LEDs off ASAP
}
